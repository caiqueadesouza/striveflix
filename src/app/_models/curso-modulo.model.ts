import { Curso } from "./curso.model";
import { Modulo } from "./modulo.model";

export class CursoModulo {
    id: number = 0;
    modulo_id: number[] = [];
    curso_id: number = 0;
    modulo: Modulo = new Modulo();
    curso: Curso = new Curso()
}
