import { Modulo } from "./modulo.model";

export class Curso {
    id: number = 0;
    titulo: string = '';
    descricao: string = '';
    logo: string = '';
    inicio: string = '';
    fim: string = ''
    publicar_em: string = '';
    valor: string = '';
    tag: string = '';
    status: boolean = true;
    modulos: Modulo = new Modulo()
}
