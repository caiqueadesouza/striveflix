import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvaliacoesUpdateComponent } from './avaliacoes-update.component';

describe('AvaliacoesUpdateComponent', () => {
  let component: AvaliacoesUpdateComponent;
  let fixture: ComponentFixture<AvaliacoesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvaliacoesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvaliacoesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
