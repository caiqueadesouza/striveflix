import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvaliacoesIndexComponent } from './avaliacoes-index/avaliacoes-index.component';
import { AvaliacoesStoreComponent } from './avaliacoes-store/avaliacoes-store.component';
import { AvaliacoesUpdateComponent } from './avaliacoes-update/avaliacoes-update.component';
import { Route, RouterModule } from '@angular/router';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';

const avaliacoesRoutes: Route[] = [
    {
        path: 'index',
        component: AvaliacoesIndexComponent
    },
    {
        path: 'store',
        component: AvaliacoesStoreComponent
    },
    {
        path: 'update',
        component: AvaliacoesUpdateComponent
    }
];

@NgModule({
    declarations: [
        AvaliacoesIndexComponent,
        AvaliacoesStoreComponent,
        AvaliacoesUpdateComponent
    ],
    imports: [
        CommonModule,
        HeaderPageModule,
        RouterModule.forChild(avaliacoesRoutes)
    ]
})
export class AvaliacoesModule { }
