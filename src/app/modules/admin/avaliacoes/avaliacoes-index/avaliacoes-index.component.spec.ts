import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvaliacoesIndexComponent } from './avaliacoes-index.component';

describe('AvaliacoesIndexComponent', () => {
  let component: AvaliacoesIndexComponent;
  let fixture: ComponentFixture<AvaliacoesIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvaliacoesIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvaliacoesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
