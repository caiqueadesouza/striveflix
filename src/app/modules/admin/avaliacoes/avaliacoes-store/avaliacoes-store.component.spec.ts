import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvaliacoesStoreComponent } from './avaliacoes-store.component';

describe('AvaliacoesStoreComponent', () => {
  let component: AvaliacoesStoreComponent;
  let fixture: ComponentFixture<AvaliacoesStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvaliacoesStoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvaliacoesStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
