import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlunoIndexComponent } from './aluno-index/aluno-index.component';
import { AlunoUpdateComponent } from './aluno-update/aluno-update.component';
import { Route, RouterModule } from '@angular/router';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

const alunoRoutes: Route[] = [
    {
        path: 'index',
        component: AlunoIndexComponent
    },
    {
        path: 'update',
        component: AlunoUpdateComponent
    }
];

@NgModule({
    declarations: [
        AlunoIndexComponent,
        AlunoUpdateComponent
    ],
    imports: [
        CommonModule,
        HeaderPageModule,
        MatPaginatorModule,
        MatTableModule,
        RouterModule.forChild(alunoRoutes)
    ]
})
export class AlunoModule { }
