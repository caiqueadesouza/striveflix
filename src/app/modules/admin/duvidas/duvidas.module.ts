import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DuvidasIndexComponent } from './duvidas-index/duvidas-index.component';
import { DuvidasUpdateComponent } from './duvidas-update/duvidas-update.component';
import { DuvidasStoreComponent } from './duvidas-store/duvidas-store.component';
import { Route, RouterModule } from '@angular/router';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';

const duvidasRoutes: Route[] = [
    {
        path: 'index',
        component: DuvidasIndexComponent
    },
    {
        path: 'store',
        component: DuvidasStoreComponent
    }    ,
    {
        path: 'update',
        component: DuvidasUpdateComponent
    }
];

@NgModule({
    declarations: [
        DuvidasIndexComponent,
        DuvidasUpdateComponent,
        DuvidasStoreComponent
    ],
    imports: [
        CommonModule,
        HeaderPageModule,
        RouterModule.forChild(duvidasRoutes)
    ]
})
export class DuvidasModule { }
