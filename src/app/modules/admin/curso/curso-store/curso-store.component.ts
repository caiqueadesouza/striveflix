import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Curso } from 'app/_models/curso.model';
import { CursoService } from 'app/_services/curso.service';

@Component({
    selector: 'app-curso-store',
    templateUrl: './curso-store.component.html',
    styleUrls: ['./curso-store.component.scss']
})
export class CursosStoreComponent implements OnInit {

    form: FormGroup;
    curso: Curso = new Curso();

    constructor(
        private formBuilder: FormBuilder,
        private cursoService: CursoService,
        private datePipe: DatePipe,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.buildForm();
    }

    buildForm() {
        this.form = this.formBuilder.group({
            titulo: ['', Validators.required],
            descricao: ['', Validators.required],
            inicio: ['', Validators.required],
            fim: ['', Validators.required],
            publicar_em: [''],
            valor: ['', Validators.required],
            limitado: [''],
        });
    }

    get f() {
        return this.form.controls;
    }

    onSubmit() {
        this.curso.logo = 'teste';
        if (this.form.valid) {
            this.form.get('limitado').value ? this.curso.tag = 'limitado' : '';

            this.curso.inicio = this.datePipe.transform(this.curso.inicio, 'yyyy-MM-dd HH:mm:ss', 'UTC');
            this.curso.fim = this.datePipe.transform(this.curso.fim, 'yyyy-MM-dd HH:mm:ss', 'UTC');
            this.curso.publicar_em = this.datePipe.transform(this.curso.publicar_em, 'yyyy-MM-dd HH:mm:ss', 'UTC');

            this.cursoService.create(this.curso).subscribe(
                (res) => {
                    this.cursoService.showMessage('Curso criado com sucesso!');
                    const id = res.id;
                    this.router.navigate(['/curso/modulos', id]);
                }
            );
        } else {
            this.markFormGroupTouched(this.form);
        }
    }

    markFormGroupTouched(formGroup: FormGroup) {
        Object.values(formGroup.controls).forEach(control => {
            if (control instanceof FormGroup) {
                this.markFormGroupTouched(control);
            } else {
                control.markAsTouched();
            }
        });
    }

    uploadAvatar(fileList: FileList): void {

        if (!fileList.length) {
            return;
        }

        const allowedTypes = ['image/jpeg', 'image/png'];
        const file = fileList[0];

        if (!allowedTypes.includes(file.type)) {
            return;
        }

        // this._contactsService.uploadAvatar(this.contact.id, file).subscribe();
    }

}
