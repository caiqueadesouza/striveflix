import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Curso } from 'app/_models/curso.model';
import { CursoService } from 'app/_services/curso.service';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ModuloAddCursoComponent } from '../../modulo/modulo-add-curso/modulo-add-curso.component';

@Component({
  selector: 'app-curso-update',
  templateUrl: './curso-update.component.html',
  styleUrls: ['./curso-update.component.scss']
})
export class CursosUpdateComponent implements OnInit {

  form: FormGroup;
  curso!: Curso;

  isDrawerOpen = false;
  constructor(
    private formBuilder: FormBuilder,
    private cursoService: CursoService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.isDrawerOpen = !this.isDrawerOpen;
    this.curso = new Curso();
    this.carregarCurso();
    this.buildForm();
  }

  get f() {
    return this.form.controls;
  }

  buildForm() {
    this.form = this.formBuilder.group({
      titulo: ['', Validators.required],
      descricao: ['', Validators.required],
      inicio: ['', Validators.required],
      fim: ['', Validators.required],
      publicar_em: [''],
      valor: ['', Validators.required],
      limitado: [''],
    });
  }

  carregarCurso() {
    let paramId: String = this.route.snapshot.paramMap.get('id') ?? '';
    this.cursoService.readById(+paramId).subscribe((curso) => {
      this.curso = curso;
    });
  }

  onSubmit() {
    this.curso.logo = 'teste';
    if (this.form.valid) {
      this.form.get('limitado').value ? this.curso.tag = 'limitado' : '';

      this.curso.inicio = this.datePipe.transform(this.curso.inicio, 'yyyy-MM-dd HH:mm:ss', 'UTC');
      this.curso.fim = this.datePipe.transform(this.curso.fim, 'yyyy-MM-dd HH:mm:ss', 'UTC');
      this.curso.publicar_em = this.datePipe.transform(this.curso.publicar_em, 'yyyy-MM-dd HH:mm:ss', 'UTC');

      this.cursoService.create(this.curso).subscribe(
        (res) => {
          this.cursoService.showMessage('Curso atualizado com sucesso!');
        }
      );
    } else {
      this.markFormGroupTouched(this.form);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach(control => {
      if (control instanceof FormGroup) {
        this.markFormGroupTouched(control);
      } else {
        control.markAsTouched();
      }
    });
  }

  addModulo(): void {
    const dialogRef = this.dialog.open(ModuloAddCursoComponent, {
      panelClass: 'app-full-bleed-dialog',
      data: { id: this.curso.id }
    });
  }

}
