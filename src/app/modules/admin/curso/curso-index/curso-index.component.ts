import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { BasePagination } from 'app/_models/base-pagination.model';
import { Curso } from 'app/_models/curso.model';
import { CursoService } from 'app/_services/curso.service';
import {
    debounceTime,
    map,
    distinctUntilChanged,
    filter,
    switchMap,
} from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { CursoModulosComponent } from '../curso-modulos/curso-modulos.component';
import { MatDialog } from '@angular/material/dialog';
import { Modulo } from 'app/_models/modulo.model';


@Component({
    selector: 'app-curso-index',
    templateUrl: './curso-index.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CursosIndexComponent implements OnInit {

    @ViewChild('searchInput', { static: true }) searchInput!: ElementRef;

    displayedColumns: string[] = [
        'id',
        'titulo',
        'alunos',
        'tag',
        'modulos',
        'status',
        'editar',
        'excluir'
    ];

    cursos!: BasePagination<Curso>;
    pageEvent!: PageEvent;
    pageIndex: number = 0;
    palavra: string = '';
    ativo: boolean = null;

    constructor(
        private cursoService: CursoService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        this.carregarDados(this.palavra);
        this.buscaPalavra();
    }

    buscaPalavra() {
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                map((event: Event) => (event.target as HTMLInputElement).value),
                filter((res) => res.length > 2 || res.length === 0),
                debounceTime(400),
                distinctUntilChanged(),
                switchMap((text: string) => this.cursoService.readPaginationSearch(this.pageIndex + 1, text, this.ativo))
            )
            .subscribe((cursos) => {
                this.cursos = cursos;
            });
    }

    pageNavigations(event: PageEvent) {
        this.pageIndex = event.pageIndex;
        this.carregarDados(this.palavra);
    }

    carregarDados(palavra: string) {
        this.cursoService
            .readPaginationSearch(this.pageIndex + 1, palavra, this.ativo)
            .subscribe((cursos) => {
                this.cursos = cursos;
            });
    }

    getModulos(curso: Curso): void {
        const dialogRef = this.dialog.open(CursoModulosComponent, {
            panelClass: 'app-full-bleed-dialog',
            data: {
                titulo: curso.titulo,
                modulos: curso.modulos
            }
        });
    }
}
