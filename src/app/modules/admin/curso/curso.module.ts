import { MatStepperModule } from '@angular/material/stepper';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CursosIndexComponent } from './curso-index/curso-index.component';
import { CursosStoreComponent } from './curso-store/curso-store.component';
import { CursosUpdateComponent } from './curso-update/curso-update.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { CommonModule, DatePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSidenavModule } from '@angular/material/sidenav';
import { StatusModule } from 'app/shared/componentes/status/status.module';
import { TagModule } from 'app/shared/componentes/tag/tag.module';
import { CursoModulosComponent } from './curso-modulos/curso-modulos.component';
import { MatBadgeModule } from '@angular/material/badge';


const cursoRoutes: Route[] = [
    {
        path: 'index',
        component: CursosIndexComponent
    },
    {
        path: 'store',
        component: CursosStoreComponent
    },
    {
        path: 'update/:id',
        component: CursosUpdateComponent
    }
];

@NgModule({
    declarations: [
        CursosIndexComponent,
        CursosStoreComponent,
        CursosUpdateComponent,
        CursoModulosComponent
    ],
    providers: [DatePipe],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatStepperModule,
        HeaderPageModule,
        MatIconModule,
        SharedModule,
        MatButtonModule,
        MatSlideToggleModule,
        MatPaginatorModule,
        MatTableModule,
        MatSidenavModule,
        StatusModule,
        TagModule,
        MatBadgeModule,
        RouterModule.forChild(cursoRoutes)
    ]
})
export class CursoModule {
}
