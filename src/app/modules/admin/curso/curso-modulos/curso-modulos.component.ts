import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Modulo } from 'app/_models/modulo.model';

export interface DialogData {
  titulo: string;
  modulos: Modulo
}

@Component({
  selector: 'app-curso-modulos',
  templateUrl: './curso-modulos.component.html',
  styleUrls: ['./curso-modulos.component.scss']
})
export class CursoModulosComponent implements OnInit {

  displayedColumns: string[] = ['id', 'titulo', 'aulas', 'editar'];

  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<CursoModulosComponent>,
  ) { }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close();
  }

}
