import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { BasePagination } from 'app/_models/base-pagination.model';
import { Modulo } from 'app/_models/modulo.model';
import { ModuloService } from 'app/_services/modulo.service';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter,
  switchMap,
} from 'rxjs/operators';
import { fromEvent } from 'rxjs';


@Component({
  selector: 'app-modulo-index',
  templateUrl: './modulo-index.component.html',
  styleUrls: ['./modulo-index.component.scss']
})
export class ModuloIndexComponent implements OnInit {

  @ViewChild('searchInput', { static: true }) searchInput!: ElementRef;

  displayedColumns: string[] = [
    'id',
    'titulo',
    'aulas',
    'status',
    'editar',
    'excluir'
  ];

  modulos!: BasePagination<Modulo>;
  pageEvent!: PageEvent;
  pageIndex: number = 0;
  palavra: string = '';
  ativo: boolean = null

  constructor(private moduloService: ModuloService) { }

  ngOnInit(): void {
    this.carregarDados(this.palavra);
    this.buscaPalavra();
  }

  buscaPalavra() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: Event) => (event.target as HTMLInputElement).value),
        filter((res) => res.length > 2 || res.length === 0),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap((text: string) => this.moduloService.readPaginationSearch(this.pageIndex + 1, text, this.ativo))
      )
      .subscribe((modulos) => {
        this.modulos = modulos;
      });
  }

  pageNavigations(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.carregarDados(this.palavra);
  }

  carregarDados(palavra: string) {
    this.moduloService
      .readPaginationSearch(this.pageIndex + 1, palavra, this.ativo)
      .subscribe((modulos) => {
        this.modulos = modulos;
      });
  }

}
