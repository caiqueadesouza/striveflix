import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Modulo } from 'app/_models/modulo.model';
import { ModuloService } from 'app/_services/modulo.service';

@Component({
  selector: 'app-modulo-store',
  templateUrl: './modulo-store.component.html',
  styleUrls: ['./modulo-store.component.scss']
})
export class ModuloStoreComponent implements OnInit {

  form: FormGroup;
  modulo: Modulo = new Modulo();

  constructor(
    private formBuilder: FormBuilder,
    private moduloService: ModuloService,
    public dialogRef: MatDialogRef<ModuloStoreComponent>,
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      titulo: ['', Validators.required],
      descricao: [''],
    });
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    if (this.form.valid) {
      this.moduloService.create(this.modulo).subscribe(
        () => {
          this.moduloService.showMessage('Módulo criado com sucesso!');
          this.close();
        }
      );
    } else {
      this.markFormGroupTouched(this.form);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach(control => {
      if (control instanceof FormGroup) {
        this.markFormGroupTouched(control);
      } else {
        control.markAsTouched();
      }
    });
  }

  close(): void {
    this.dialogRef.close();
  }

}
