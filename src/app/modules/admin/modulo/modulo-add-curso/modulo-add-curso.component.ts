import { SelectionModel } from '@angular/cdk/collections';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { BasePagination } from 'app/_models/base-pagination.model';
import { Modulo } from 'app/_models/modulo.model';
import { ModuloService } from 'app/_services/modulo.service';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter,
  switchMap,
} from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { ModuloStoreComponent } from '../modulo-store/modulo-store.component';
import { CursoModulo } from 'app/_models/curso-modulo.model';
import { CursoService } from 'app/_services/curso.service';

export interface DialogData {
  id: number;
}

@Component({
  selector: 'app-modulo-add-curso',
  templateUrl: './modulo-add-curso.component.html',
  styleUrls: ['./modulo-add-curso.component.scss']
})
export class ModuloAddCursoComponent implements OnInit {

  @ViewChild('searchInput', { static: true }) searchInput!: ElementRef;
  displayedColumns: string[] = ['select', 'id', 'titulo', 'aulas', 'editar'];

  selection = new SelectionModel<Modulo>(true, []);

  modulos!: BasePagination<Modulo>;
  pageEvent!: PageEvent;
  pageIndex: number = 0;
  palavra: string = '';
  ativo: boolean = true;

  cursoModulo: CursoModulo = new CursoModulo();

  constructor(
    public dialogRef: MatDialogRef<ModuloAddCursoComponent>,
    private moduloService: ModuloService,
    private cursoService: CursoService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit(): void {
    this.carregarDados(this.palavra);
    this.buscaPalavra();
  }

  buscaPalavra() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: Event) => (event.target as HTMLInputElement).value),
        filter((res) => res.length > 2 || res.length === 0),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap((text: string) => this.moduloService.readPaginationSearch(this.pageIndex + 1, text, this.ativo))
      )
      .subscribe((modulos) => {
        this.modulos = modulos;
      });
  }

  pageNavigations(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.carregarDados(this.palavra);
  }

  carregarDados(palavra: string) {
    this.moduloService
      .readPaginationSearch(this.pageIndex + 1, palavra, this.ativo)
      .subscribe((modulos) => {
        this.modulos = modulos;
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.modulos.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.modulos.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Modulo): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  novoModulo(): void {
    this.close();
    const dialogRef = this.dialog.open(ModuloStoreComponent, {
      panelClass: 'app-full-bleed-dialog',
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.cursoModulo.curso_id = this.data.id;
    this.cursoModulo.modulo_id = this.selection.selected.map(item => item.id);
    this.cursoService.updateModulos(this.cursoModulo).subscribe(
      (res) => {
        this.cursoService.showMessage('Módulos adicionados com sucesso!');
      }
    );
  }

}
