import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuloStoreComponent } from './modulo-store/modulo-store.component';
import { ModuloIndexComponent } from './modulo-index/modulo-index.component';
import { ModuloUpdateComponent } from './modulo-update/modulo-update.component';
import { Route, RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from 'app/shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';
import { StatusModule } from 'app/shared/componentes/status/status.module';
import { ModuloAddCursoComponent } from './modulo-add-curso/modulo-add-curso.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SearchFormModule } from 'app/shared/componentes/search-form/search-form.module';

const moduloRoutes: Route[] = [
  {
    path: 'index',
    component: ModuloIndexComponent
  }
];

@NgModule({
  declarations: [
    ModuloStoreComponent,
    ModuloIndexComponent,
    ModuloUpdateComponent,
    ModuloAddCursoComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    SharedModule,
    MatPaginatorModule,
    MatTableModule,
    HeaderPageModule,
    StatusModule,
    MatCheckboxModule,
    SearchFormModule,
    RouterModule.forChild(moduloRoutes)
  ]
})
export class ModuloModule { }
