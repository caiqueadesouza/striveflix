import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AulaIndexComponent } from './aula-index/aula-index.component';
import { AulaStoreComponent } from './aula-store/aula-store.component';
import { AulaUpdateComponent } from './aula-update/aula-update.component';
import { Route, RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from 'app/shared/shared.module';
import { HeaderPageModule } from 'app/shared/componentes/header-page/header-page.module';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { StatusModule } from 'app/shared/componentes/status/status.module';

const aulaRoutes: Route[] = [
  {
    path: 'index',
    component: AulaIndexComponent
  },
  {
    path: 'store',
    component: AulaStoreComponent
  },
  {
    path: 'update/:id',
    component: AulaUpdateComponent
  }
];

@NgModule({
  declarations: [
    AulaIndexComponent,
    AulaStoreComponent,
    AulaUpdateComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    HeaderPageModule,
    SharedModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    StatusModule,
    RouterModule.forChild(aulaRoutes)
  ]
})
export class AulaModule { }
