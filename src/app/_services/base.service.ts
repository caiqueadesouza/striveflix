import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map, catchError } from 'rxjs/operators';
import { EMPTY, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService<T, Y> {
  baseUrl = 'http://localhost/denilson/striveflix-api/public/api';
  errorMessage: string = '';

  constructor(
    protected snackBar: MatSnackBar,
    protected http: HttpClient
  ) { }

  readPaginationSearch(page: number, palavra: String, ativo: boolean): Observable<Y> {
    return this.http
      .get<Y>(`${this.baseUrl}/list?page=${page}&palavra=${palavra}&ativo=${ativo}`)
      .pipe(
        map((obj) => obj),
        catchError((e) => this.errorHandler(e))
      );
  }

  read(): Observable<T[]> {
    return this.http.get<T[]>(`${this.baseUrl}/index`).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  create(objeto: T): Observable<T> {
    return this.http.post<T>(`${this.baseUrl}/store`, objeto).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  readById(id: number): Observable<T> {
    const url = `${this.baseUrl}/show/${id}`;
    return this.http.get<T>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  update(objeto: T, id: number): Observable<T> {
    const url = `${this.baseUrl}/update/${id}`;
    return this.http.post<T>(url, objeto).pipe(
      map((obj) => obj),
      catchError((e: any) => {
        this.showMessage(e, true);
        return EMPTY;
      })
    );
  }

  delete(id: number): Observable<T> {
    const url = `${this.baseUrl}/delete/${id}`;
    return this.http.delete<T>(url).pipe(
      map((obj) => obj),
      catchError((e: any) => {
        this.showMessage(e, true);
        return EMPTY;
      })
    );
  }

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'X', {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success'],
    });
  }

  errorHandler(e: any): Observable<any> {
    this.showMessage(e, true);
    return throwError(e);
  }
}
