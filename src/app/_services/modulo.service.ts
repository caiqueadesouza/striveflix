import { Injectable } from '@angular/core';
import { Modulo } from 'app/_models/modulo.model';
import { BaseService } from './base.service';
import { BasePagination } from 'app/_models/base-pagination.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ModuloService extends BaseService<Modulo, BasePagination<Modulo>> {

  constructor(snackBar: MatSnackBar, http: HttpClient) {
    super(snackBar, http);
  }

  override baseUrl = `http://localhost/denilson/striveflix-api/public/api/modulo`
}
