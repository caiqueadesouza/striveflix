import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BaseService } from './base.service';
import { Curso } from 'app/_models/curso.model';
import { BasePagination } from 'app/_models/base-pagination.model';
import { CursoModulo } from 'app/_models/curso-modulo.model';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CursoService extends BaseService<Curso, BasePagination<Curso>> {

  constructor(snackBar: MatSnackBar, http: HttpClient) {
    super(snackBar, http);
  }

  override baseUrl = `http://localhost/denilson/striveflix-api/public/api/curso`

  updateModulos(cursoModulo: CursoModulo): Observable<CursoModulo> {
    return this.http.post<CursoModulo>(`${this.baseUrl}/curso-modulos`, cursoModulo).pipe(
      map((obj) => obj),
      catchError((e: any) => {
        this.showMessage(e, true);
        return EMPTY;
      })
    );
  }

}
