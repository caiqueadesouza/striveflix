/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'dashboards.analytics',
                title: 'Dashboard',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/dashboard'
            },
        ]
    },
    {
        id: 'cursos',
        title: 'Cursos',
        type: 'group',
        children: [
            {
                id: 'apps.ecommerce',
                title: 'Cursos',
                type: 'collapsable',
                icon: 'heroicons_outline:academic-cap',
                children: [
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Cursos',
                        type: 'basic',
                        link: '/curso/index'
                    },
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Novo Curso',
                        type: 'basic',
                        link: '/curso/store'
                    }
                ]
            },
            {
                id: 'modulos',
                title: 'Módulos',
                type: 'collapsable',
                icon: 'heroicons_outline:view-grid-add',
                children: [
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Módulos',
                        type: 'basic',
                        link: '/modulo/index'
                    },
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Novo Módulo',
                        type: 'basic',
                        link: '/modulo/store'
                    }
                ]
            },
            {
                id: 'aulas',
                title: 'Aulas',
                type: 'collapsable',
                icon: 'heroicons_outline:book-open',
                children: [
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Aulas',
                        type: 'basic',
                        link: '/aula/index'
                    },
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Nova Aula',
                        type: 'basic',
                        link: '/aula/store'
                    }
                ]
            }
        ]
    },
    {
        id: 'alunos',
        title: 'Alunos',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'dashboards.analytics',
                title: 'Alunos',
                type: 'basic',
                icon: 'heroicons_solid:users',
                link: '/aluno/index'
            },
        ]
    },
    {
        id: 'configuracoes',
        title: 'Configurações',
        type: 'group',
        children: [
            {
                id: 'apps.ecommerce',
                title: 'Dúvidas Frequentes',
                type: 'collapsable',
                icon: 'heroicons_solid:chat-alt',
                children: [
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Dúvidas',
                        type: 'basic',
                        link: '/duvidas-frequentes/index'
                    },
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Novo Dúvida',
                        type: 'basic',
                        link: '/duvidas-frequentes/store'
                    }
                ]
            },
            {
                id: 'apps.ecommerce',
                title: 'Avaliações de Alunos',
                type: 'collapsable',
                icon: 'heroicons_outline:academic-cap',
                children: [
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Avaliações',
                        type: 'basic',
                        link: '/avaliacoes/index'
                    },
                    {
                        id: 'apps.ecommerce.inventory',
                        title: 'Nova Avaliação',
                        type: 'basic',
                        link: '/avaliacoes/store'
                    }
                ]
            }
        ]
    },
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
