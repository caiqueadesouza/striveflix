import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderPageComponent } from './header-page.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        HeaderPageComponent
    ],
    exports: [HeaderPageComponent],
    imports: [
        CommonModule,
        MatIconModule
    ]
})
export class HeaderPageModule { }
