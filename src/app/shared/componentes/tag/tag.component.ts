import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {
  @Input() tag!: string;
  tagName: string;
  background: string;
  color: string;

  constructor() { }

  ngOnInit(): void {
    switch (this.tag) {
      case 'lotado':
        this.tagName = 'Lotade';
        this.background = 'bg-purple-200'
        this.color = 'text-purple-800'
        break;
      case 'limitado':
        this.tagName = 'Limitado';
        this.background = 'bg-orange-200'
        this.color = 'text-orange-800'
        break;
      case 'ultimas_vagas':
        this.tagName = 'Ultimas Vagas';
        this.background = 'bg-indigo-200'
        this.color = 'text-indigo-800'
        break;
      default:
        break;
    }

  }
}
