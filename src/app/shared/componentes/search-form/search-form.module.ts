import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SearchFormComponent } from './search-form.component';



@NgModule({
  declarations: [SearchFormComponent],
  exports: [SearchFormComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class SearchFormModule { }
